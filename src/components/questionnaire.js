import React from 'react'
import { List, Avatar, Button } from 'antd';


const Questionnaries = (props) => {
    return(
      <List
        itemLayout="vertical"
        size="large"
        pagination={{
          onChange: (page) => {
            console.log(page);
          },
          pageSize: 3,
        }}
        dataSource={props.data}
        renderItem={item => (
          <List.Item
            key={item.name}
          >
            <List.Item.Meta
              avatar={<Avatar src='https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png' />}
              description={item.name}
            />
            {item.description}
            <div>
                <Button type="primary" href={`http://127.0.0.1:3000/questionnaire/question/${item.question_uuid}`}>
                    Start
                </Button>
            </div>
          </List.Item>
        )}
      />
    );
};

export default Questionnaries;