import React from 'react';
import axios from 'axios'
import {Button} from 'antd'
import {Redirect} from 'react-router-dom'


class QuestionView extends React.Component {

    state = {
        answer: {}
    }

    componentDidMount() {
        axios.get(`http://127.0.0.1:8000${this.props.match.url}`, {withCredentials: true})
            .then(res => {
                this.setState({
                   answer: res.data
                })
                console.log(res.data)
            })
    }

    render() {
        if (this.state.answer.question_destination_uuid === null) {
            return (
                <div>
                    <p>{this.state.answer.additional_text}</p>
                    <Button type="primary" href={`http://127.0.0.1:3000/`}>
                        Ok
                    </Button>
                </div>

            )
        } else if (this.state.answer.question_destination_uuid != null) {
            return (
                <Redirect to={`/questionnaire/question/${this.state.answer.question_destination_uuid}`}/>
            )

        } else {
            return(<p></p>)
        }

    }
}

export  default QuestionView;