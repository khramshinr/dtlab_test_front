import React from 'react';
import axios from 'axios'
import {Button, List} from 'antd'


class QuestionView extends React.Component {

    state = {
        question: {}
    }

    componentDidMount() {
        axios.get(`http://127.0.0.1:8000${this.props.match.url}`, {withCredentials: true})
            .then(res => {
                this.setState({
                   question: res.data
                })
            })
    }

    render() {
        return (
        <List
            header={<div>{this.state.question.text}</div>}
            bordered
            dataSource={this.state.question.answers}
            renderItem={item => (
            <List.Item>
                <Button type="primary" href={`http://127.0.0.1:3000/questionnaire/answer/${item.uuid}`}>
                    {item.text}
                </Button>
            </List.Item>)}
        />
        )
    }
}

export  default QuestionView;