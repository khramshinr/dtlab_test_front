import React from 'react';
import axios from 'axios'

import Questionnaries from '../components/questionnaire'


class QuestionnarieList extends React.Component {

    state = {
        questionnaries: []
    }

    componentDidMount() {
        axios.get('http://127.0.0.1:8000/questionnaire/', {withCredentials: true})
            .then(res => {
                this.setState({
                   questionnaries: res.data
                })
            })
    }

    render() {
        return (
            <Questionnaries data={this.state.questionnaries}/>
        )
    }
}

export  default QuestionnarieList;