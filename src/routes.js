import React from 'react';
import {Route} from "react-router-dom";

import Answer from './containers/answerView'
import Question from './containers/questionView'
import QuestionnarieList from './containers/questionnarieListView'


const BaseRouter = () => (
    <div>
        <Route exact path='/' component={QuestionnarieList} />
        <Route exact path='/questionnaire/question/:uuid' component={Question} />
        <Route exact path='/questionnaire/answer/:uuid' component={Answer} />
    </div>
);

export default BaseRouter;